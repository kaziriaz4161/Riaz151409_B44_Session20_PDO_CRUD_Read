-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 05, 2017 at 08:57 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomic_project_b44`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE `birthday` (
  `id` int(11) NOT NULL,
  `name` varchar(115) COLLATE utf8_unicode_ci NOT NULL,
  `birthday` date NOT NULL,
  `soft_delete` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `name`, `birthday`, `soft_delete`) VALUES
(1, 'Shamim', '1993-05-13', 'No'),
(2, 'Shakil', '1993-05-14', 'No'),
(3, 'Asad', '1991-05-15', 'Yes'),
(4, 'Asif', '1995-06-14', 'Yes'),
(6, 'Arif', '2015-05-13', 'No'),
(0, 'Riaz', '2017-02-05', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE `book_title` (
  `id` int(11) NOT NULL,
  `book_name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `author_name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_deleted` varchar(11) COLLATE utf8_unicode_ci DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_name`, `author_name`, `soft_deleted`) VALUES
(1, 'Pyramid', 'Tom Martin', 'No'),
(2, 'Inferno', 'Dan Brown', 'No'),
(3, 'Himu', 'Humayun Ahmed', 'Yes'),
(4, 'vcchg', 'fdsfd', 'No'),
(5, 'gdfgd', '547646', 'Yes'),
(6, 'ertdy', '346477', 'No'),
(7, 'fdsd fs', 'sfd sf df', 'No'),
(8, 'Durbin', 'Shirshendu', 'Yes'),
(9, 'sfds', 'sdfertwt3', 'No'),
(10, 'dfs', 'fds sar', 'No'),
(11, 'sdfsf', 'sdfsg', 'No'),
(12, 'Mr Y', 'Mr X', 'No'),
(13, 'fdgdfg', 'dsfgdsdyr', 'No'),
(14, 'stest', 'fdsgdsg', 'No'),
(15, 'sdfsfs', 'sfdsf', 'No'),
(16, 'sdgfs', 'sdgf', 'No'),
(17, 'dfs fd', 'sdf sgt ert', 'No'),
(18, 'Satkahon', 'Somres Majumdar', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `city_name`
--

CREATE TABLE `city_name` (
  `id` int(11) NOT NULL,
  `name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `city_name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_deleted` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `city_name`
--

INSERT INTO `city_name` (`id`, `name`, `city_name`, `soft_deleted`) VALUES
(0, 'Kazi Riaz', 'Chittagong', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(11) NOT NULL,
  `name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_deleted` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `name`, `email`, `soft_deleted`) VALUES
(0, 'Riaz', '', 'No'),
(0, 'Riaz', '', 'No'),
(0, 'Riaz', '', 'No'),
(0, 'Kaniz', '', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` int(11) NOT NULL,
  `name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_deleted` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `name`, `gender`, `soft_deleted`) VALUES
(0, 'Riaz', '', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE `hobbies` (
  `id` int(11) NOT NULL,
  `name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `hobbies` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_deleted` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `name`, `hobbies`, `soft_deleted`) VALUES
(0, 'Riaz', '', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `organization_summary`
--

CREATE TABLE `organization_summary` (
  `id` int(11) NOT NULL,
  `organization_name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `organization_summary` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_deleted` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `organization_summary`
--

INSERT INTO `organization_summary` (`id`, `organization_name`, `organization_summary`, `soft_deleted`) VALUES
(0, 'BITM', '', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

CREATE TABLE `profile_picture` (
  `id` int(11) NOT NULL,
  `name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `picture_address` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_deleted` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `profile_picture`
--

INSERT INTO `profile_picture` (`id`, `name`, `picture_address`, `soft_deleted`) VALUES
(0, 'Riaz', '', 'No');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
