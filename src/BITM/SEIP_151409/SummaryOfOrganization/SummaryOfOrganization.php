<?php


namespace App\SummaryOfOrganization;

use App\Message\Message;
use App\Utility\Utility;


use App\Model\Database as DB;
use PDO;

class SummaryOfOrganization extends DB{
    public $id="";
    public $organization_name="";
    public $organization_summary="";



    public function __construct(){
        parent::__construct();
    }

    public function setData($data=null){

        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('organization_name',$data)){
            $this->organization_name=$data['organization_name'];

        }
        if(array_key_exists('organization_summary',$data)){
            $this->organization_summary=$data['organization_summary'];

        }
    }


    public function store(){

        $arrData = array($this->organization_name,$this->organization_summary);

        $sql = "INSERT into organization_summary(organization_name,organization_summary) VALUES(?,?)";

        $STH = $this->DBH->prepare($sql);

        $result =$STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :( ");

        Utility::redirect('create.php');


    }

    public function index(){

        $sql = "select * from organization_summary where soft_deleted='No'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }


    public function view(){

        $sql = "select * from organization_summary where id=".$this->id;

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetch();

    }


    public function trashed(){

        $sql = "select * from organization_summary where soft_deleted='Yes'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }


}