<?php


namespace App\Hobbies;

use App\Message\Message;
use App\Utility\Utility;


use App\Model\Database as DB;
use PDO;

class Hobbies extends DB{
    public $id="";
    public $name="";
    public $hobbies="";



    public function __construct(){
        parent::__construct();
    }

    public function setData($postData){

        if(array_key_exists('id',$postData)){
            $this->id = $postData['id'];
        }

        if(array_key_exists('name',$postData)){
            $this->name = $postData['name'];
        }

        if(array_key_exists('hobbies',$postData)){
            $this->birthday = $postData['hobbies'];
        }

    }

    public function store(){

        $arrData = array($this->name,$this->hobbies);

        $sql = "INSERT into hobbies(name,hobbies) VALUES(?,?)";

        $STH = $this->DBH->prepare($sql);

        $result =$STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :( ");

        Utility::redirect('create.php');


    }

    public function index(){

        $sql = "select * from hobbies where soft_deleted='No'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }


    public function view(){

        $sql = "select * from hobbies where id=".$this->id;

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetch();

    }


    public function trashed(){

        $sql = "select * from hobbies where soft_deleted='Yes'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }


}