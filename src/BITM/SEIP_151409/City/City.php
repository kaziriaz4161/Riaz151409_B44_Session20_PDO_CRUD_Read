<?php


namespace App\City;
use App\Message\Message;
use App\Utility\Utility;


use App\Model\Database as DB;
use PDO;


class City extends DB{
    public $id="";
    public $name="";
    public $city_name="";

    public function __construct(){
        parent::__construct();
    }

    public function setData($postData){

        if(array_key_exists('id',$postData)){
            $this->id = $postData['id'];
        }

        if(array_key_exists('name',$postData)){
            $this->name = $postData['name'];
        }

        if(array_key_exists('city_name',$postData)){
            $this->city_name = $postData['city_name'];
        }

    }

    public function store(){

        $arrData = array($this->name,$this->city_name);

        $sql = "INSERT into city_name(name,city_name) VALUES(?,?)";

        $STH = $this->DBH->prepare($sql);

        $result =$STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :( ");

        Utility::redirect('create.php');


    }

    public function index(){

        $sql = "select * from city_name where soft_deleted='No'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }


    public function view(){

        $sql = "select * from city_name where id=".$this->id;

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetch();

    }


    public function trashed(){

        $sql = "select * from city_name where soft_deleted='Yes'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }




}